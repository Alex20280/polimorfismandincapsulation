public class Main {
    public static void main(String[] args) {

        Shelf shelf = new Shelf();


        Shelf.findOldestBook();

        System.out.println();

        shelf.findBookWithAuthor("George Martin");

        System.out.println();

        shelf.printBookOlderThen(1992);

        System.out.println();

        shelf.findBooksWithAuthorAndCoauthor("Someone");

    }
}
