package model;

public class Book {
    public String title;
    public String author;
    public String publisher;
    public Integer yearOfPublishing;

    public void print() {
        System.out.println("Книги изданные ранее указанного года: ");
        System.out.println("Название: " + this.title + "\nавтор " + this.author + "\nИздатель" + this.publisher + "\nгод публикации" + this.yearOfPublishing);
    }
}
